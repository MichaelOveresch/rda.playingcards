
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Hearts, Spades, Diamonds, Clubs };

struct Card
{
    Rank Rank;
    Suit Suit;
};

void PrintCard(Card card1)
{
    int rank1;

    switch (card1.Rank)
    {
    case Two: rank1 = 2;
        cout << "The Two of ";
        break;
    case Three: rank1 = 3;
        cout << "The Three of ";
        break;
    case Four: rank1 = 4;
        cout << "The Four of ";
        break;
    case Five: rank1 = 5;
        cout << "The Five of ";
        break;
    case Six: rank1 = 6;
        cout << "The Six of ";
        break;
    case Seven: rank1 = 7;
        cout << "The Seven of ";
        break;
    case Eight: rank1 = 8;
        cout << "The Eight of ";
        break;
    case Nine: rank1 = 9;
        cout << "The Nine of ";
        break;
    case Ten: rank1 = 10;
        cout << "The Ten of ";
        break;
    case Jack: rank1 = 11;
        cout << "The Jack of ";
        break;
    case Queen: rank1 = 12;
        cout << "The Queen of ";
        break;
    case King: rank1 = 13;
        cout << "The King of ";
        break;
    case Ace: rank1 = 14;
        cout << "The Ace of ";
        break;
    }
    switch (card1.Suit)
    {
    case Hearts: cout << "Hearts.\n";
        break;
    case Diamonds: cout << "Diamonds.\n";
        break;
    case Clubs: cout << "Clubs.\n";
        break;
    case Spades: cout << "Spades.\n";
        break;

    }

}

Card HighCard(Card card1, Card card2)
{
    int rank1, rank2;

    string suit1 = "";
    string face1 = "";
    string suit2 = "";
    string face2 = "";

    switch (card1.Rank)
    {
    case Two: rank1 = 2;
        face1 = "The Two of ";
        break;
    case Three: rank1 = 3;
        face1 = "The Three of ";
        break;
    case Four: rank1 = 4;
        face1 = "The Four of ";
        break;
    case Five: rank1 = 5;
        face1 = "The Five of ";
        break;
    case Six: rank1 = 6;
        face1 = "The Six of ";
        break;
    case Seven: rank1 = 7;
        face1 = "The Seven of ";
        break;
    case Eight: rank1 = 8;
        face1 = "The Eight of ";
        break;
    case Nine: rank1 = 9;
        face1 = "The Nine of ";
        break;
    case Ten: rank1 = 10;
        face1 = "The Ten of ";
        break;
    case Jack: rank1 = 11;
        face1 = "The Jack of ";
        break;
    case Queen: rank1 = 12;
        face1 = "The Queen of ";
        break;
    case King: rank1 = 13;
        face1 = "The King of ";
        break;
    case Ace: rank1 = 14;
        face1 = "The Ace of ";
        break;
    }
    switch (card1.Suit)
    {
    case Hearts: suit1 = "Hearts";
        break;
    case Diamonds: suit1 = "Diamonds.\n";
        break;
    case Clubs: suit1 = "Clubs.\n";
        break;
    case Spades: suit1 = "Spades.\n";
        break;
    }


    switch (card2.Rank)
    {
    case Two: rank2 = 2;
        face2 = "The Two of ";
        break;
    case Three: rank2 = 3;
        face2 = "The Three of ";
        break;
    case Four: rank2 = 4;
        face2 = "The Four of ";
        break;
    case Five: rank2 = 5;
        face2 = "The Five of ";
        break;
    case Six: rank2 = 6;
        face2 = "The Six of ";
        break;
    case Seven: rank2 = 7;
        face2 = "The Seven of ";
        break;
    case Eight: rank2 = 8;
        face2 = "The Eight of ";
        break;
    case Nine: rank2 = 9;
        face2 = "The Nine of ";
        break;
    case Ten: rank2 = 10;
        face2 = "The Ten of ";
        break;
    case Jack: rank2 = 11;
        face2 = "The Jack of ";
        break;
    case Queen: rank2 = 12;
        face2 = "The Queen of ";
        break;
    case King: rank2 = 13;
        face2 = "The King of ";
        break;
    case Ace: rank2 = 14;
        face2 = "The Ace of ";
        break;
    }
    switch (card2.Suit)
    {
    case Hearts: suit2 = "Hearts.\n";
        break;
    case Diamonds: suit2 = "Diamonds.\n";
        break;
    case Clubs: suit2 = "Clubs.\n";
        break;
    case Spades: suit2 = "Spades.\n";
        break;
    }

    if (rank1 < rank2)
    {
        face2 + suit2;
    }
    if (rank2 < rank1)
    {
        face1 + suit1;
    }

    return card1, card2;
}

int main()
{
    Card card1;
    Card card2;

    card1.Suit = Spades;
    card1.Rank = Two;

    card2.Rank = Jack;
    card2.Suit = Hearts;

    PrintCard(card1);

    HighCard(card1, card2);

    (void)_getch();
    return 0;
}